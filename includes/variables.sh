#!/bin/bash

readonly WORKSPACE_FOLDER="/Users/raph/Workspace/Simplon"
readonly WORKSPACE_USER="raph"
readonly APACHE_SITE_DIR="/etc/apache2/sites-available"
readonly APACHE_CMD_ENSITE="a2ensite"
readonly APACHE_CMD_RELOAD="apachectl restart"