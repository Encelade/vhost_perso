#!/bin/bash

#
# Colors
#
RED=$'\e[31;01m'
BLUE=$'\e[36;01m'
YELLOW=$'\e[33;01m'
NORMAL=$'\e[0m'
GREEN=$'\e[32;01m'

#
# Loging functions
#
info() {
    echo "${YELLOW}${1}${NORMAL}" >&2;
}

notice() {
    echo "${BLUE}${1}${NORMAL}" >&2;
}

success() {
    echo "${GREEN}${1}${NORMAL}" >&2;
}

error() {
    echo "${RED}${1}${NORMAL}" >&2;
}
