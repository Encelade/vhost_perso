#!/bin/bash

# Récupération du dossier ou se trouve le script
SRC=$(cd $(dirname "$0"); pwd)

# Include helpers & variables
. $SRC/includes/helpers.sh
. $SRC/includes/variables.sh

# Vérifier si le script est lancé en sudo
if [[ $EUID -ne 0 ]]; then
   error "Le script doit être lancé en sudo. Merci!"
   exit 1
fi

# On demande le nom du projet
notice "Enter the project name: "
read project_name

# Vérifier la présence de caractères interdits
if grep -q "[[:punct:]]" <<< "$project_name"
then 
    echo "Caractère interdit"
    exit 1
fi

# On créer l'url local du projet
readonly project_url="$project_name.local"

# On créer le dossier de travail et on copie le template html
info "Creation du dossier de travail..."
webroot="$WORKSPACE_FOLDER/$project_name/"
mkdir -p $webroot
cp -rf $SRC/templates/html/*.png $webroot
sed -e "s@{{PROJECT}}@$project_name@" $SRC/templates/html/index.html > $webroot/index.html

# On remet les bons droits
chown -R $WORKSPACE_USER $webroot

# On créer le fichier de configuration du vhost
info "Creation du vhost apache..."
default_vhost="$SRC/templates/apache/vhost-default.conf"
sed -e "s@{{SERVER_NAME}}@$project_url@" -e "s@{{WEB_ROOT}}@$webroot@" $default_vhost > $APACHE_SITE_DIR/$project_name.conf

# On rajouter le nom serveur dans le fichier /etc/hosts
info "Modification du fichier hosts..."
echo -e "127.0.0.1\t$project_url" >> /etc/hosts

# On active le vhost apache
info "Activation du vhost..."
$APACHE_CMD_ENSITE $APACHE_SITE_DIR/$project_name.conf > /dev/null 2>&1

# On reload apache
info "Reload de apache..."
$APACHE_CMD_RELOAD > /dev/null 2>&1

# Done!
success "Le vhost http://$project_url/ a bien été créé!"